<?php

class Registro extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('registro_model','reg');
	}

    public function index()
	{
		$data= array();
        $data['title']='IATec - Histórias';
		$data['enviar'] = base_url('registro/registro2');
	    $this->parser->parse('registro/index',$data);
	}

    public function registro2(){
        $data= $this->input->post();
		if($this->reg->registro($data)){
			$this->session->set_flashdata('s','Registro concluído!');
			redirect(base_url('index'));
		}else{
			$this->session->set_flashdata('n','Registro não concluído!');
			redirect(base_url('registro/index'));
		}
	}
}