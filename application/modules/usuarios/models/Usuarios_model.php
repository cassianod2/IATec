<?php

class Usuarios_model extends CI_Model {
//	protected $table = 'usuarios';
	protected $primaryKey = 'idUsuarios';

	public function __construct()
	{
		parent::__construct();
		$this->load->library('table');
	}
	
	public function login($array){
		$login=$this->db->where('login',$array['login'])->get('usuario');
		if($login->num_rows() >0){
			$all = $this->db->where($array)->get('usuario');

			if($all->num_rows()>0){
				$data = $all->result_array()[0];
                $data['logado']=true;
				unset($data['senha']);
				$this->session->set_userdata('usuario',$data);
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function listahistorias(){
		
        $template = array('table_open' => '<table class="table table-bordered table-striped">');

        $this->table->set_template($template);

        $this->table->set_heading('ID','Título','Data Inclusão','<center>Questão</center>','Excluir');
        
        $historia=$this->db->get('historia');

		foreach($historia->result() as $index){
			$urladd = base_url("usuarios/addquestao/{$index->id}");
			$urldel = base_url("usuarios/delhistoria/{$index->id}");
			$this->table->add_row($index->id, $index->titulo,$index->dtinc, 
			"<center><a class='btn btn-success center' href='{$urladd}'><span class='glyphicon glyphicon-plus'></span></a></center>",
			"<center><a class='btn btn-danger center' href='{$urldel}'><span class='glyphicon glyphicon-trash'></span></a></center>"
			);
        }
        
        return $this->table->generate();
	}
	
	public function salvahistoria($array){
		if($this->db->insert('historia',$array)){
			return true;
		}else{
			return false;
		}
	}
	public function excluirhist($id){
		$a=$this->db->get('historia');
	
		if($a->num_rows() > 0){
			if($this->db->where('id',$id)->delete('historia')){
				$q = $this->db->select('id')->where('idhistoria',$id)->get('questao');
				if($q->num_rows() > 0){
					foreach($q->result() as $index){
						$this->db->where('idquestao',$index->id)->delete('alternativa');
					}
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
			
		}
	}
	
	public function listaquestao($id){
	
	    $template = array('table_open' => '<table class="table table-bordered table-striped">');
	
	    $this->table->set_template($template);
	
	    $this->table->set_heading('ID','Questão','Data Inclusão','Excluir');
	    
	    $historia=$this->db->where('idhistoria',$id)->get('questao');
	
			foreach($historia->result() as $index){
				$urldel = base_url("usuarios/delquestao/{$index->id}");
				$this->table->add_row($index->id, $index->questao,$index->dtinc, 
				"<center><a class='btn btn-danger center' href='{$urldel}'><span class='glyphicon glyphicon-trash'></span></a></center>"
				);
		    }
	    
	    return $this->table->generate();
	}
	
	public function addquestao($array){
		
		if(!empty($array['certa1'])){
			$array['certa1']= 1;
		}else{
				$array['certa1']= 0;
		}
		
		
		if(!empty($array['certa2'])){
			$array['certa2']= 1;
		}else{
				$array['certa2']= 0;
		}
		
		if(!empty($array['certa3'])){
			$array['certa3']= 1;
		}else{
				$array['certa3']= 0;
		}
		
		if(!empty($array['certa4'])){
			$array['certa4']= 1;
		}else{
				$array['certa4']= 0;
		}
		
		$this->db->set('idhistoria',$array['idhistoria'])->set('questao',$array['questao']);
		if($this->db->insert('questao')){
			$id = $this->db->insert_id();
			$this->db->set('idquestao',$id)->set('opcao',$array['resposta1'])->set('correta',$array['certa1'])->insert('alternativa');
			$this->db->set('idquestao',$id)->set('opcao',$array['resposta2'])->set('correta',$array['certa2'])->insert('alternativa');
			$this->db->set('idquestao',$id)->set('opcao',$array['resposta3'])->set('correta',$array['certa3'])->insert('alternativa');
			$this->db->set('idquestao',$id)->set('opcao',$array['resposta4'])->set('correta',$array['certa4'])->insert('alternativa');
	
			return true;
		}else{
			return false;
		}
		
	}
	
	public function delquestao($id){
		$q=$this->db->where('id',$id)->get('questao');
	
		if($q->num_rows() > 0){
			if($this->db->where('id',$id)->delete('questao')){
				$this->db->where('idquestao',$id)->delete('alternativa');
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
		
	}
	
}
