
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{title}</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/css">
        .body{
            margin:10px;
        }
    </script>

</head>
<br>
<body class="body">
<div class="row">
    <div class="col-sm-2" >
    </div>
    <div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title text-center"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> IATec - Lista histórias</h3>
            </div>
            <div class="panel-body">
                {tabela}
                <br>
                <a class="btn btn-success" href="{addhistoria}">Adicionar História</a>
                <a class="btn btn-info right" href="{url}">Sair</a>
            </div>
            
        </div>
    </div>
    <div class="col-sm-2">
    </div>

</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
</body>
</html>