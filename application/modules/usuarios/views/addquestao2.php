
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{title}</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/css">
        .body{
            margin:10px;
        }
    </script>

</head>
<br>
<body class="body">
<div class="row">
    <div class="col-sm-2" >
    </div>
    <div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title text-center"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> IATec - Adicionando Questões</h3>
            </div>
            <div class="panel-body">
                 <form action="{salvar}" method="post" role="form">
                     <input type="hidden" value="{idhistoria}" name="idhistoria">
                    <div class="form-group">
                        <label for="Questão">Questão</label>
                        <input type="text" class="form-control" id="questao" name="questao" placeholder="Questão">
                    </div>
                   <br>
                    <div class="form-group">
                      <label class="control-label col-sm-2" for="resposta1">Alternativa 1:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="resposta1" name="resposta1" placeholder="Resposta" required>
                       <div class="checkbox">
                          <label><input type="checkbox" name="certa1"> Alternativa certa</label>
                        </div>
                      </div>
                    </div>
                   
                    <div class="form-group">
                      <label class="control-label col-sm-2" for="resposta2">Alternativa 2:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="resposta2" name="resposta2" placeholder="Resposta" required>
                       <div class="checkbox">
                          <label><input type="checkbox" name="certa2"> Alternativa certa</label>
                        </div>
                      </div>
                    </div>
                
                <div class="form-group">
                      <label class="control-label col-sm-2" for="resposta3">Alternativa 3:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="resposta3" name="resposta3" placeholder="Resposta" required>
                       <div class="checkbox">
                          <label><input type="checkbox" name="certa3"> Alternativa certa</label>
                        </div>
                      </div>
                    </div>
                
                <div class="form-group">
                      <label class="control-label col-sm-2" for="resposta4">Alternativa 4:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="resposta4" name="resposta4" placeholder="Resposta" required>
                       <div class="checkbox">
                          <label><input type="checkbox" name="certa4"> Alternativa certa</label>
                        </div>
                      </div>
                    </div>

                    <button class="btn btn-success" type="submit">Salvar</button>
                    <a class="btn btn-info right" href="{voltar}">Voltar</a>
                </form>
            </div>
            
        </div>
    </div>
    <div class="col-sm-2">
    </div>

</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
</body>
</html>