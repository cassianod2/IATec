<?php

class Usuarios extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('usuarios_model','user');
	}

    public function index()
	{
	    $data = array();
		$data['title'] = 'IATec Login';
		$data['enviar']= base_url('usuarios/logar');		
		$data['cadastro']= base_url('registro');
		

		$this->parser->parse('usuarios/index',$data);
	}

	function logar(){
		$data=$this->input->post();
		if($this->user->login($data)){
			redirect(base_url("usuarios/historia"));
		}else{
			redirect(base_url("usuarios/index"));
		}
	}

	public function historia(){
		if($this->session->userdata('usuario')['logado'] != true){
			redirect(base_url("usuarios/index"));
		}
		$data = array();
		$data['title'] = 'IATec - Adicionar e Remover história';
		$data['addhistoria'] = base_url('usuarios/addhistoria');
		$data['url'] = base_url('usuarios/sair');
		$data['tabela']= $this->user->listahistorias();
		$this->parser->parse('usuarios/historia',$data);
	}
	
	public function addhistoria(){
				$data = array();
		$data['title'] = 'IATec - Adicionando história';
		$data['voltar'] = base_url("usuarios/historia");
		$data['salvar'] = base_url('usuarios/addhistoria2');

		$this->parser->parse('usuarios/addhistoria',$data);
	}
	
	public function addhistoria2(){
		$data = $this->input->post(null,true);
		
		if($this->user->salvahistoria($data)){
			$this->session->set_flashdata('s','História Registrada!');
			redirect(base_url("usuarios/historia"));
		}else{
			$this->session->set_flashdata('n','História não Registrada!');
			redirect(base_url("usuarios/historia"));
		}	
	}
	
	public function delhistoria($id){
		if($this->user->excluirhist($id)){
			$this->session->set_flashdata('s','História Deletada!');
			redirect(base_url("usuarios/historia"));
		}else{
			redirect(base_url("usuarios/historia"));
		}
	}
	
	public function addquestao($id){
		$data = array();
		$data['title'] = 'IATec - Adicionando questão';
		$data['voltar'] = base_url("usuarios/historia");
		$data['tabela']= $this->user->listaquestao($id);
		$data['addquestao'] = base_url("usuarios/addquestao2/{$id}");
		$this->parser->parse('usuarios/addquestao',$data);
	}
	
	public function addquestao2($id){
		$data = array();
		$data['idhistoria']=$id;
		$data['voltar']= base_url("usuarios/addquestao/{$id}");
		$data['title'] = 'IATec - Adicionando questão';
		$data['salvar'] = base_url('usuarios/addquest');
		$this->parser->parse('usuarios/addquestao2',$data);
	}
	
	function addquest(){
		$data= $this->input->post();
		if($this->user->addquestao($data)){
			$this->session->set_flashdata('s','História Deletada!');
			redirect(base_url("usuarios/historia"));
		}else{
			redirect(base_url("usuarios/historia"));
		}
	}
	
	public function delquestao($id){
		if($this->user->delquestao($id)){
			$this->session->set_flashdata('s','História Deletada!');
			redirect(base_url("usuarios/historia"));
		}else{
			redirect(base_url("usuarios/historia"));
		}
	}
	
	public function sair(){
		if($this->session->sess_destroy()){
			$this->session->set_flashdata('s','Saída efetuada com sucesso!');
			redirect(base_url("index"));
		}else{
			redirect(base_url("index"));
		}
	}
}