<?php

class Index_model extends CI_Model {
	//protected $table = 'index';
	protected $primaryKey = 'idIndex';

	public function __construct()
	{
		parent::__construct();
	}
	
	public function contos(){
	    $template = array('table_open' => '<table class="table table-bordered table-striped">');
	
	    $this->table->set_template($template);
	
	    $this->table->set_heading("Contos e Histórias");
	    
	    $historia=$this->db->get('historia');
	
			foreach($historia->result() as $index){
				$urldel = base_url("index/historia/{$index->id}");
				$this->table->add_row("<a href='{$urldel}'>{$index->titulo}</a>");
		    }
	    
	    return $this->table->generate();
	
	}
	
	function getConto($id){
		return $this->db->where('id',$id)->get('historia')->result();
	}
	public function perguntas($id){
	    $template = array('table_open' => '<table class="table table-bordered table-striped">');
	
	    $this->table->set_template($template);
	
	    $this->table->set_heading("Questões sobre o texto: <small>Clique sobre o link para ir para a questão.</small>");
	    
	    $historia=$this->db->where('idhistoria',$id)->get('questao');
	
			foreach($historia->result() as $index){
				$urldel = base_url("index/questao/{$index->id}/{$id}");
				$this->table->add_row("<a href='{$urldel}'>{$index->questao}</a>");
		    }
	    
	    return $this->table->generate();
	
	}
	public function questao($id){
		return $this->db->where('id',$id)->get('questao')->result();
	}
	
	public function alternativas($id){
		return $this->db->where('idquestao',$id)->get('alternativa')->result();
	}
	public function verificaq($data){
	$resposta = $this->db->where('id',$data['certa'])->get('alternativa');
		if($resposta->num_rows()>0){
			$c = $resposta->result()[0];
			if($c->correta == 1){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}