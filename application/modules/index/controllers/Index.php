<?php

class Index extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('index_model','index');
	}

    public function index()
	{
		$data = array();
		$data['title'] = 'IATec - Contos e Histórias';
		$data['tabela'] = $this->index->contos();
		$data['admin'] = base_url('usuarios');
		$this->parser->parse('index/index',$data);
	}
	
	public function historia($id){
		$data = array();
		$data['title'] = 'IATec - Contos e Histórias';
	    $conto = $this->index->getConto($id)[0];
	    $data['titulo'] = $conto->titulo;
	    $data['autor'] = $conto->autor;
	    $data['conto'] = $conto->historia;
		$data['perguntas'] = $this->index->perguntas($id);
		$data['voltar'] = base_url('index');
		
		$this->parser->parse('index/historia',$data);
	}
	
	public function questao($id,$idhistoria){
		$data = array();
		$data['title'] = 'IATec - Contos e Histórias (Questões)';
		$questao = $this->index->questao($id)[0];
		$data['questao'] = $questao->questao;
		$data['alternativas'] = $this->index->alternativas($id);
		$data['enviar']= base_url("index/verificaquestao/{$idhistoria}");
		$data['voltar'] = base_url("index/historia/{$idhistoria}");
		
		$this->parser->parse('index/questao',$data);
	}

	public function verificaquestao($id){
		$data= $this->input->post(null,true);

		if($this->index->verificaq($data)){
			$this->session->set_flashdata('s',"Parabéns você acertou :D");
			redirect(base_url("index/historia/{$id}"));
		}else{
			$this->session->set_flashdata('n',"Ops...você errou :(");
			redirect(base_url("index/historia/{$id}"));
		}
	}
}