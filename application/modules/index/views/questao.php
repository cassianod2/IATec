
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{title}</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/css">
        .body{
            margin:10px;
        }
    </script>

</head>
<br>
<body class="body">
<div class="row">
    <div class="col-sm-1" >
    </div>
    <div class="col-sm-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title text-center"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> IATec - Contos e Histórias(Respondendo Questão)</h3>
            </div>
            <div class="panel-body">
                <b>Pergunta: {questao}</b><br>
                
                <p>
                <form method="post" role="form" action="{enviar}">
                    <?php foreach($alternativas as $index):?>
                    <p><input type="checkbox" name="certa" value="<?php echo $index->id;?>"><?php echo $index->opcao; ?>  </p><br>
                    <?php endforeach;?>
                    <br>
                    <button class="btn btn-success" type="submit">Enviar</button>
                </form>
                </p>
                
                
                <br>
                <a href="{voltar}" class="btn btn-info">Voltar</a>
            </div>
        </div>
    </div>
    <div class="col-sm-1">
    </div>

</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
</body>
</html>